## api-blog
Sujet du blog: Notre dépendance des écrans.
Une plateforme de blog en node/react avec une gestion des utilisateurs.

## Fontionnalités

- Un user doit pouvoir s'inscrire,
- Se connecter 
- Une fois connecter le user peut poster des articles sur son espace personnel.
- N'importe qui peut consulter les articles des autres user.

# Use Case 
![wireframe usecase](img/usercaseBlog.png)
# user stories
![wireframe userHistory](img/userHistory.png)
# Diagrame des classes
![wireframe userHistory](img/Diagrame-classe-blog.png)
# Maquette fonctionnelle
![wireframe maquette](img/blog.png)
![wireframe maquette3](img/blog2.png)
![wireframe maquette2](img/blog1.png)

## Thechnologies utilisées

* BDD : MySql
* Api : Node/express
* FrameWorks front-end : react
* Authentification : JWT

**lien**
https://gitlab.com/henda85/api-blog

https://gitlab.com/henda85/blog-react
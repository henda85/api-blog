


--table user
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE `email` (`email`)
);
--table arcicle
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id_article` int NOT NULL AUTO_INCREMENT,
  `titre` varchar(30) DEFAULT NULL,
  `contenu` varchar(1000) DEFAULT NULL ,
  `date` DATE  NOT NULL,
  `userId_fk` int NOT NULL,
  PRIMARY KEY (`id_article`)
  
); 

ALTER TABLE article 
  ADD CONSTRAINT 
  FOREIGN KEY (userId_fk)
  REFERENCES user(id_user)
  ON DELETE RESTRICT
  ON UPDATE CASCADE
;
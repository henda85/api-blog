import { Router } from "express";
import passport from "passport";
import { Article } from "../entity/Article";
import { ArticleRepository } from "../repository/ArticleRepository"; 

export const articleController = Router() ;

articleController.get('/',async(req,res)=>{
    const articles = await ArticleRepository.findAll();
    res.json(articles);
})

articleController.post('/',passport.authenticate('jwt', {session:false}),async(req,res)=>{
        try {
            console.log(req.user);
            let article=  new Article();
            Object.assign(article,req.body);
            article.userIdFk=req.user.idUser;
            await ArticleRepository.add(article);
            res.status(201).json(article); 
        } catch (error) {
            console.log(error);
            res.status(500).end();
        }

})
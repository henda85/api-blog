import { Router } from "express";
import { User } from "../entity/User";
import { UserRepository } from "../repository/UserRepository";
import  bcrypt from 'bcrypt';
import {generateToken} from "../utils/token.js";
import passport from "passport";



export const userControler = Router();

userControler.get('/',async(req,res)=>{
    const users = await UserRepository.findAll();
    res.json(users);
})

userControler.post('/',async(req,res)=>{
    try {
        const newUser= new User();
        Object.assign(newUser,req.body);
        const exist = await UserRepository.findByEmail(newUser.email);
        if(exist){
            res.status(400).json({error:'Ce compte existe déjà'});
            return;
        }
        newUser.role = "user";
        newUser.password = await bcrypt.hash(newUser.password,12);
        await UserRepository.add(newUser);
        res.status(201).json({
            user:newUser,
            token: generateToken({
                                    email: newUser.email,
                                    id:newUser.idUser,
                                    nom:newUser.nom
            })
        })
        
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})
userControler.post('/login', async(req,res)=>{
    try{
        const user = await UserRepository.findByEmail(req.body.email);
        if(user) {
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            if(samePassword) {
                res.json({
                    user,
                    token: generateToken({
                        email: user.email,
                        id:user.idUser,
                        nom:user.nom
                    })
                });
                return;
            }
        }
        res.status(401).json({error:'henda Wrong email and/or password'});

    }catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
});


userControler.get('/account', passport.authenticate('jwt', {session:false}),(req, res)=>{
    res.json(req.user);
}) 



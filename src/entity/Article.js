export class Article{
    idArticle='';
    titre='';
    contenue='';
    date='';
    userIdFk='';
    constructor( idArticle, titre, contenue, date, userIdFk){
        this.idArticle = idArticle;
        this.titre = titre;
        this.contenue = contenue;
        this.date = date;
        this.userIdFk = userIdFk;
    }
}
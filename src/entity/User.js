export class User{
    idUser = '';
    nom = '';
    email = '';
    password = '';
    role = '';
    constructor(nom, email , password , role, idUser =null){
        this.idUser = idUser;
        this.nom = nom;
        this.email = email;
        this.password = password;
        this.role = role;
    } 
}
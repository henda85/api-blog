import { connection } from "./connection";
import { Article } from "../entity/Article";


export class ArticleRepository{

    /**
     * Method that gets all articles from database
     * @returns {Promise<article[]>} 
     */
    static async findAll(){
        const [rows] = await connection.query('SELECT * FROM article');
        return rows.map(row => new Article(row['id_article'], row['titre'], row['contenu'],row['date'] ,row['userId_fk'])); 
    }

    /**
     * Method that post articles in the database
     * @param {Article} article à ajouter
     */
    static async add(article){
        const [rows] = await connection.query(' INSERT INTO article( titre, contenu, date, userId_fk) values(?,?,?,?)',[article.titre, article.contenue, article.date,article.userIdFk]);
        article.idArticle = rows.insertId;
    }
} 
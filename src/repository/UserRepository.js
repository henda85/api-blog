import { connection } from "./connection";
import { User } from "../entity/User"



export class UserRepository{

    static async findAll(){
        const [rows]= await connection.query('SELECT * FROM user');
        return rows.map(row => new User(row['nom'], row['email'], row['password'],row['role'] ,row['idUser'])); 
    }

    /**
     * une methode qui ajoute un user
     * @param {User} user user à ajouter
     */
    static async add (user){
        const [rows]= await connection.query('INSERT INTO user (nom, email, password, role) VALUES (?, ?, ?, ?)',[user.nom, user.email, user.password, user.role]);
        user.idUser = rows.insertId;
    }
    /**
     * une methode qui returne un user specific apartir d'un email
     * @param {string} email
     * @returns {Promise<>User}
     */
    static async findByEmail(email){
        const [rows] = await connection.query('select * from user WHERE email = ?',[email]);
        if (rows.length===1){
            return new User(rows[0].nom, rows[0].email, rows[0].password,rows[0].role, rows[0].id_user);
        }
        return null;
    }
}

import express from 'express';
import cors from 'cors';
import { userControler } from './controler/userControler';
import {  articleController } from './controler/articleController'
import {configurePasseport } from './utils/token';
import passport from 'passport';
export const server = express();

//On appel la fonction qui configure la stratégie JWT
configurePasseport();

server.use(passport.initialize());

server.use(express.json());
server.use(cors());
server.use('/api/user',userControler);
server.use('/api/article',articleController);
